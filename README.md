### In short about your future Junior Java team member 🦦

The most interesting part of building my projects for me was designing backend systems in Java. And refactoring *of course*😍 

I am looking for a backend Java position, where I will be able to grow and deepen the art of a system design.

### Projects flex

![Playlist Manager](https://gitlab.com/uploads/-/system/project/avatar/19053075/cover.jpg){width=25 height=25px} **Playlist Manager:**
 Spotify plugin for playlist superusers
[website](https://playlistmanager.10cats.com) | [gitlab source code](https://gitlab.com/viraono/playlist-manager)

![Arlanda planner Android App](https://gitlab.com/uploads/-/system/project/avatar/23937025/arlandaapp.png){width=25 height=25px} **Arlanda planner Android App:** laziest trip planner: just enter your flight destination [gitlab source code](https://gitlab.com/viraono/arlanda-planner)

![What Is Russia](https://gitlab.com/uploads/-/system/project/avatar/55695277/questionmark.jpg){width=25 height=25px} **What Is Russia:** months of my historical research for the truth [whatisrussia.org](https://whatisrussia.org) 

*you can also find a convient summary of my projects on* [my portfolio page](https://https://portfolio.10cats.com)